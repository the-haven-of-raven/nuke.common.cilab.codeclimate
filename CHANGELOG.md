# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [vNext]

## [0.2.1] - 2024-12-06
## Changed
- Output conversion now always produces unix-like path for the report

## [0.2.0] - 2023-11-26
## Changed
- Updated references & .NET 8

## [0.1.9] - 2021-11-24
## Changed
- Warnings file path is now relative (fixes fingerprint between different workspaces)
- Warnings project path is now relative (fixes fingerprint between different workspaces)

## [0.1.8] - 2021-11-24
## Changed
- Warnings file path is now relative

## [0.1.7] - 2021-10-28
## Changed
- CodePage is now optional, files are written using UTF-8 without BOM by default

## [0.1.6] - 2021-05-21
## Changed
- Fix missing enum value DO_NOT_SHOW

## [0.1.5] - 2021-05-20
### Added
- InspectCode issues as CodeClimate report

## [0.1.3] - 2020-11-19
### Changed
- Prettify json output

## [0.1.2] - 2020-11-17
### Changed
- Fix settings serialization
- Fix encoding serialization

## [0.1.1] - 2020-11-16
### Changed
- Fix Serialization issue

## [0.1.0] - 2020-11-06
### Added
- Dotnet warnings as CodeClimate report