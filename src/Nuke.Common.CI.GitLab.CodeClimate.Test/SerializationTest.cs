using System.Collections.Generic;
using System.Text.Json;
using Nuke.Common.Tooling;
using Xunit;

namespace Nuke.Common.CI.GitLab.CodeClimate.Test;

public class SerializationTest
{
    [Fact]
    public void SettingsNewInstance()
    {
        var settings = new CodeClimateSettings();
        var newSettings = settings.NewInstance();

        Xunit.Assert.Equal(settings, newSettings);
    }

    public static IEnumerable<object[]> SerializableClasses =>
        new List<object[]>
        {
            new object[] { new Begin() },
            new object[] { new Issue() },
            new object[] { new Lines() },
            new object[] { new Location() },
            new object[] { new Positions() }
        };

    [Theory]
    [MemberData(nameof(SerializableClasses))]
    public void BeginSerialization(object obj)
    {
        var serialized = JsonSerializer.Serialize(obj);

        var deserialized = JsonSerializer.Deserialize(serialized, obj.GetType());

        Xunit.Assert.Equal(obj, deserialized);
    }
}

