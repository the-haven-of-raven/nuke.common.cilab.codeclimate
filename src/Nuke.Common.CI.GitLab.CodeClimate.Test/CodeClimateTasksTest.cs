﻿using FluentAssertions.Json;
using Nuke.Common.Tooling;
using System.Reflection;

namespace Nuke.Common.CI.GitLab.CodeClimate.Test;

public class CodeClimateTasksTest
{
    [Fact]
    public void ConvertOutputToCodeQuality_Win_and_Unix_Should_Match()
    {
        List<Output> outputLinux = [
            new Output {
                Text = "/builds/foo/bar/src/Project/Foobar.cs(221,5): warning NU5104: Some warning!!! [/builds/foo/bar/src/Project/Project.csproj]",
                Type = OutputType.Std,
            }];

        List<Output> outputWindows = [
            new Output {
                Text = "C:\\builds\\foo\\bar\\src\\Project\\Foobar.cs(221,5): warning NU5104: Some warning!!! [C:\\builds\\foo\\bar\\src\\Project\\Project.csproj]",
                Type = OutputType.Std,
            }];

        var jsonLinux = CodeClimateTasks.ConvertOutputToCodeQuality(outputLinux, new OutputCodeClimateSettings
        {
            Regex = RegularExpressions.DotNetBuildWarning,
            OutputFile = string.Empty,
            RootDirectory = "/builds/foo/bar"
        });

        jsonLinux.Should().NotBeNullOrEmpty();
        jsonLinux.Should().MatchSnapshot();

        var jsonWindows = CodeClimateTasks.ConvertOutputToCodeQuality(outputWindows, new OutputCodeClimateSettings
        {
            Regex = RegularExpressions.DotNetBuildWarning,
            OutputFile = string.Empty,
            RootDirectory = "C:\\builds\\foo\\bar"
        });

        jsonWindows.Should().NotBeNullOrEmpty();
        jsonWindows.Should().MatchSnapshot();
    }
}
