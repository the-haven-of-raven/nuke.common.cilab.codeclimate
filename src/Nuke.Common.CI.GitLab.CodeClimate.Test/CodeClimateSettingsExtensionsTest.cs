﻿using Xunit;

namespace Nuke.Common.CI.GitLab.CodeClimate.Test;

public class CodeClimateSettingsExtensionsTest
{
    [Fact]
    public void SetSeverityForCodeTest()
    {
        var target = new OutputCodeClimateSettings();
        var actual = target.SetSeverityForCode("NU1567", Severity.Blocker);

        Xunit.Assert.True(actual.SeverityMap.TryGetValue("NU1567", out var actualValue));
        Xunit.Assert.Equal(Severity.Blocker, actualValue);

        actual = actual.SetSeverityForCode("NU1568", Severity.Critical);

        Xunit.Assert.True(actual.SeverityMap.TryGetValue("NU1568", out actualValue));
        Xunit.Assert.Equal(Severity.Critical, actualValue);
        Xunit.Assert.True(actual.SeverityMap.TryGetValue("NU1567", out actualValue));
        Xunit.Assert.Equal(Severity.Blocker, actualValue);
    }
}

