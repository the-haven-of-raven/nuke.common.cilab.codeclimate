﻿using System.Text.Json.Serialization;

namespace Nuke.Common.CI.GitLab.CodeClimate;

public class Positions
{
    public Positions()
    {
        Begin = new Begin();
    }

    [JsonPropertyName("begin")]
    public Begin Begin
    {
        get;
        set;
    }

    protected bool Equals(Positions other)
    {
        return Begin.Equals(other.Begin);
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) {
            return false;
        }

        if (ReferenceEquals(this, obj)) {
            return true;
        }

        if (obj.GetType() != this.GetType()) {
            return false;
        }

        return Equals((Positions)obj);
    }

    public override int GetHashCode()
    {
        return Begin.GetHashCode();
    }
}