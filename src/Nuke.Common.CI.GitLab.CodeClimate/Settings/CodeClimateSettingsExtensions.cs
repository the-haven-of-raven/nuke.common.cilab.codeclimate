﻿using JetBrains.Annotations;
using Nuke.Common.IO;
using Nuke.Common.Tooling;

namespace Nuke.Common.CI.GitLab.CodeClimate;

[PublicAPI]
public static class CodeClimateSettingsExtensions
{

    [Pure]
    public static T SetOutputFile<T>(this T settings, string outputFile)
        where T : CodeClimateSettings
    {
        settings = settings.NewInstance();
        settings.OutputFile = outputFile;
        return settings;
    }

    [Pure]
    public static T SetCodePage<T>(this T settings, int? codePage)
        where T : CodeClimateSettings
    {
        settings = settings.NewInstance();
        settings.CodePage = codePage;
        return settings;
    }

    [Pure]
    public static T SetDefaultSeverity<T>(this T settings, Severity severity)
        where T : CodeClimateSettings
    {
        settings = settings.NewInstance();
        settings.DefaultSeverity = severity;
        return settings;
    }
}