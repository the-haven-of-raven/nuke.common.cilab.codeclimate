﻿using JetBrains.Annotations;
using Nuke.Common.Tooling;

namespace Nuke.Common.CI.GitLab.CodeClimate;

[PublicAPI]
public static class InspectCodeClimateSettingsExtensions
{
    [Pure]
    public static T SetSeverity<T>(this T settings, InspectCodeSeverity inspectCodeSeverity, Severity severity)
        where T : InspectCodeClimateSettings
    {
        settings = settings.NewInstance();
        settings.SeverityMap[inspectCodeSeverity] = severity;
        return settings;
    }

    [Pure]
    public static T SetIgnoreCSharpWarnings<T>(this T settings, bool ignore)
        where T : InspectCodeClimateSettings
    {
        settings = settings.NewInstance();
        settings.IgnoreCSharpWarnings = ignore;
        return settings;
    }

    [Pure]
    public static T EnableIgnoreCSharpWarnings<T>(this T settings)
        where T : InspectCodeClimateSettings
    {
        settings = settings.NewInstance();
        settings.IgnoreCSharpWarnings = true;
        return settings;
    }
    
    [Pure]
    public static T DisableIgnoreCSharpWarnings<T>(this T settings)
        where T : InspectCodeClimateSettings
    {
        settings = settings.NewInstance();
        settings.IgnoreCSharpWarnings = false;
        return settings;
    }
}