﻿using System;
using JetBrains.Annotations;
using Nuke.Common.Tooling;

namespace Nuke.Common.CI.GitLab.CodeClimate;

[PublicAPI]
public static class OutputCodeClimateSettingsExtensions
{
    [Pure]
    public static T SetRegex<T>(this T settings, [RegexPattern] string regex)
        where T : OutputCodeClimateSettings
    {
        if (!regex.Contains("?<filename>")) {
            throw new ArgumentException("Missing <filename> group in regex", regex);
        }

        if (!regex.Contains("?<project>")) {
            throw new ArgumentException("Missing <project> group in regex", regex);
        }

        if (!regex.Contains("?<line>")) {
            throw new ArgumentException("Missing <file> group in regex", regex);
        }

        if (!regex.Contains("?<column>")) {
            throw new ArgumentException("Missing <column> group in regex", regex);
        }

        if (!regex.Contains("?<code>")) {
            throw new ArgumentException("Missing <code> group in regex", regex);
        }

        settings = settings.NewInstance();
        settings.Regex = regex;
        return settings;
    }

    [Pure]
    public static T SetRootDirectory<T>(this T settings, string rootDirectory)
        where T : OutputCodeClimateSettings
    {
        settings = settings.NewInstance();
        settings.RootDirectory = rootDirectory;
        return settings;
    }

    [Pure]
    public static T SetSeverityForCode<T>(this T settings, string code, Severity severity)
        where T : OutputCodeClimateSettings
    {
        settings = settings.NewInstance();
        settings.SeverityMap[code] = severity;
        return settings;
    }
}