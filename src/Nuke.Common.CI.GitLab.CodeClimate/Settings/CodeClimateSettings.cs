﻿using System;
using System.Text;
using JetBrains.Annotations;
using Nuke.Common.IO;
using Nuke.Common.Tooling;

namespace Nuke.Common.CI.GitLab.CodeClimate;

[PublicAPI]
[Serializable]
public class CodeClimateSettings : ISettingsEntity
{
    public CodeClimateSettings()
    {
        OutputFile = string.Empty;
        DefaultSeverity = Severity.Info;
    }

    public virtual string OutputFile
    {
        get;
        internal set;
    }
    
    public virtual int? CodePage
    {
        get;
        internal set;
    }

    public virtual Severity DefaultSeverity
    {
        get;
        internal set;
    }

    protected bool Equals(CodeClimateSettings other)
    {
        return OutputFile == other.OutputFile && CodePage == other.CodePage && DefaultSeverity == other.DefaultSeverity;
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) {
            return false;
        }

        if (ReferenceEquals(this, obj)) {
            return true;
        }

        if (obj.GetType() != this.GetType()) {
            return false;
        }

        return Equals((CodeClimateSettings)obj);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(OutputFile, CodePage, (int)DefaultSeverity);
    }
}