﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Nuke.Common.CI.GitLab.CodeClimate;

[PublicAPI]
[Serializable]
public class OutputCodeClimateSettings : CodeClimateSettings
{
    public OutputCodeClimateSettings()
        : base()
    {
        Regex = string.Empty;
        RootDirectory = string.Empty;
        SeverityMap = new Dictionary<string, Severity>();
    }
    
    public virtual string RootDirectory
    {
        get;
        internal set;
    }

    public virtual string Regex
    {
        get;
        internal set;
    }

    public virtual Dictionary<string, Severity> SeverityMap
    {
        get;
        internal set;
    }

    protected bool Equals(OutputCodeClimateSettings other)
    {
        return base.Equals(other) && RootDirectory == other.RootDirectory && Regex == other.Regex && SeverityMap.Equals(other.SeverityMap);
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) {
            return false;
        }

        if (ReferenceEquals(this, obj)) {
            return true;
        }

        if (obj.GetType() != this.GetType()) {
            return false;
        }

        return Equals((OutputCodeClimateSettings)obj);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(base.GetHashCode(), RootDirectory, Regex, SeverityMap);
    }
}