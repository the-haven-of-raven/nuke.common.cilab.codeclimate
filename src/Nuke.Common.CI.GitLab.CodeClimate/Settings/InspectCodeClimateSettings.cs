﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Nuke.Common.CI.GitLab.CodeClimate;

[PublicAPI]
[Serializable]
public class InspectCodeClimateSettings : CodeClimateSettings
{
    public InspectCodeClimateSettings()
    {
        SeverityMap = new Dictionary<InspectCodeSeverity, Severity>();
        IgnoreCSharpWarnings = true;
    }

    public virtual Dictionary<InspectCodeSeverity, Severity> SeverityMap
    {
        get;
        internal set;
    }

    public virtual bool IgnoreCSharpWarnings
    {
        get;
        internal set;
    }

    protected bool Equals(InspectCodeClimateSettings other)
    {
        return base.Equals(other) && SeverityMap.Equals(other.SeverityMap);
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) {
            return false;
        }

        if (ReferenceEquals(this, obj)) {
            return true;
        }

        if (obj.GetType() != this.GetType()) {
            return false;
        }

        return Equals((InspectCodeClimateSettings)obj);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(base.GetHashCode(), SeverityMap);
    }
}