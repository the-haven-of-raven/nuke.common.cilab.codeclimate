﻿using System;
using System.Text.Json.Serialization;

namespace Nuke.Common.CI.GitLab.CodeClimate;

[Serializable]
public class Lines
{
    [JsonPropertyName("begin")]
    public int Begin
    {
        get;
        set;
    }

    protected bool Equals(Lines other)
    {
        return Begin == other.Begin;
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) {
            return false;
        }

        if (ReferenceEquals(this, obj)) {
            return true;
        }

        if (obj.GetType() != this.GetType()) {
            return false;
        }

        return Equals((Lines)obj);
    }

    public override int GetHashCode()
    {
        return Begin;
    }
}