﻿using System;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Nuke.Common.CI.GitLab.CodeClimate;

[Serializable]
public class Issue
{
    [JsonPropertyName("description")]
    public string Description
    {
        get;
        internal set;
    } = default!;

    [JsonPropertyName("fingerprint")]
    public string Fingerprint
    {
        get;
        internal set;
    } = default!;

    [JsonPropertyName("location")]
    public Location Location
    {
        get;
        internal set;
    } = default!;

    [JsonPropertyName("severity")]
    public Severity Severity
    {
        get;
        set;
    }

    [JsonPropertyName("check_name")]
    public string CheckName
    {
        get;
        set;
    } = default!;

    protected bool Equals(Issue other)
    {
        return Fingerprint == other.Fingerprint;
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) {
            return false;
        }

        if (ReferenceEquals(this, obj)) {
            return true;
        }

        if (obj.GetType() != GetType()) {
            return false;
        }

        return Equals((Issue)obj);
    }

    public override int GetHashCode()
    {
        return Fingerprint != null ? Fingerprint.GetHashCode() : 0;
    }
}

[JsonConverter(typeof(SeverityConverter))]
public enum Severity
{
    Disable,
    Info,
    Minor,
    Major,
    Critical,
    Blocker
}

public class SeverityConverter : JsonConverter<Severity>
{
    public override Severity Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        return Enum.Parse<Severity>(reader.GetString(), true);
    }

    public override void Write(Utf8JsonWriter writer, Severity value, JsonSerializerOptions options)
    {
        writer.WriteStringValue(value.ToString().ToLower());
    }
}