﻿using System;
using System.Text.Json.Serialization;
using JetBrains.Annotations;

namespace Nuke.Common.CI.GitLab.CodeClimate;

public class Begin
{
    [JsonPropertyName("line")]
    public int Line
    {
        [UsedImplicitly] get;
        set;
    }

    [JsonPropertyName("column")]
    public int Column
    {
        [UsedImplicitly] get;
        set;
    }

    protected bool Equals(Begin other)
    {
        return Line == other.Line && Column == other.Column;
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) {
            return false;
        }

        if (ReferenceEquals(this, obj)) {
            return true;
        }

        if (obj.GetType() != this.GetType()) {
            return false;
        }

        return Equals((Begin)obj);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Line, Column);
    }
}