﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using JetBrains.Annotations;
using Nuke.Common.CI.GitLab.CodeClimate.InspectCode;
using Nuke.Common.Tooling;
using Nuke.Common.IO;

namespace Nuke.Common.CI.GitLab.CodeClimate;

[PublicAPI]
public static class CodeClimateTasks
{
    public static void OutputToCodeClimate(IReadOnlyCollection<Output> output, OutputCodeClimateSettings? settings = null)
    {
        settings ??= new OutputCodeClimateSettings();
        var json = ConvertOutputToCodeQuality(output, settings);
        
        ((AbsolutePath)settings.OutputFile).TouchFile();

        Encoding encoding = new UTF8Encoding(false);
        if (settings.CodePage != null && Encoding.GetEncoding(settings.CodePage.Value) != null) {
            encoding = Encoding.GetEncoding(settings.CodePage.Value);
        }

        File.WriteAllText(settings.OutputFile, json, encoding);
    }

    public static void OutputToCodeClimate(IReadOnlyCollection<Output> output, Configure<OutputCodeClimateSettings> configurator)
    {
        OutputToCodeClimate(output, configurator(new OutputCodeClimateSettings()));
    }
    
    internal static string ConvertOutputToCodeQuality(IReadOnlyCollection<Output> output, OutputCodeClimateSettings settings)
    {
        var issues = new List<Issue>();
        var warningRegex = new Regex(settings.Regex);
        AbsolutePath rootDirectory = settings.RootDirectory;
        foreach (var line in output) {
            if (!warningRegex.IsMatch(line.Text)) {
                continue;
            }

            var match = warningRegex.Match(line.Text);

            AbsolutePath projectFileAbsolut = match.Groups["project"].Value;
            AbsolutePath projectDir = projectFileAbsolut.Parent;
            var relativeProjectFile = rootDirectory.GetRelativePathTo(projectFileAbsolut).ToUnixRelativePath();
            var issueFile = Path.Combine(projectDir, match.Groups["filename"].Value);
            var relativeToRoot = rootDirectory.GetRelativePathTo(issueFile).ToUnixRelativePath();
            var code = match.Groups["code"].Value;
            if (!settings.SeverityMap.TryGetValue(code, out var severity)) {
                severity = settings.DefaultSeverity;
            }

            if (severity == Severity.Disable) {
                continue;
            }

            var issue = new Issue {
                CheckName = code,
                Severity = severity,
                Description = line.Text.Replace(issueFile, relativeToRoot).Replace(projectFileAbsolut, relativeProjectFile),
                Location = new Location {
                    Path = relativeToRoot,
                    Lines = new Lines {
                        Begin = int.Parse(match.Groups["line"].Value)
                    },
                    Positions = new Positions {
                        Begin = new Begin {
                            Line = int.Parse(match.Groups["line"].Value),
                            Column = int.Parse(match.Groups["column"].Value)
                        }
                    }
                }
            };

            var jsonForFingerprint = JsonSerializer.Serialize(issue);
            issue.Fingerprint = ComputeSha1Hash(jsonForFingerprint);
            issues.Add(issue);
        }

       return JsonSerializer.Serialize(issues.Distinct(), new JsonSerializerOptions {
            WriteIndented = true
        });
    }

    public static void InspectCodeToCodeClimate(string inspectCodeFilePath, InspectCodeClimateSettings? settings = null)
    {
        settings ??= new InspectCodeClimateSettings();
        CovertInspectCodeToCodeClimate(inspectCodeFilePath, settings);
    }

    public static void InspectCodeToCodeClimate(string inspectCodeFilePath, Configure<InspectCodeClimateSettings> configurator)
    {
        InspectCodeToCodeClimate(inspectCodeFilePath, configurator(new InspectCodeClimateSettings()));
    }

    private static void CovertInspectCodeToCodeClimate(string inspectCodeFilePath, InspectCodeClimateSettings settings)
    {
        XmlSerializer serializer = new(typeof(Report));
        
        using Stream reader = new FileStream(inspectCodeFilePath, FileMode.Open);

        Report? report = (Report?)serializer.Deserialize(reader) ?? throw new System.Exception("Serialization failed");

        var issues = new List<Issue>();
        var relativeSlnPath = (RelativePath)report.Information.Solution;
        var solutionBaseDir = Path.GetDirectoryName(relativeSlnPath);
        foreach (var project in report.Issues) {
            foreach (var inspectIssue in project.Issue) {
                if (settings.IgnoreCSharpWarnings && inspectIssue.TypeId.StartsWith("CSharpWarnings")) {
                    continue;
                }
                
                var issueType = report.IssueTypes.First(x => x.Id == inspectIssue.TypeId);

                if (!settings.SeverityMap.TryGetValue(issueType.Severity, out var severity)) {
                    severity = settings.DefaultSeverity;
                }

                if (severity == Severity.Disable) {
                    continue;
                }
                var relativeFilePath = (RelativePath)inspectIssue.File;
                
                var issue = new Issue {
                    CheckName = inspectIssue.TypeId,
                    Severity = severity,
                    Description = inspectIssue.Message,
                    Location = new Location {
                        Path = string.IsNullOrEmpty(solutionBaseDir) ? relativeFilePath : Path.Combine(solutionBaseDir, relativeFilePath),
                        Lines = new Lines {
                            Begin = inspectIssue.Line
                        },
                        Positions = new Positions {
                            Begin = new Begin {
                                Line = inspectIssue.Line,
                                Column = 1
                            }
                        }
                    }
                };

                var jsonForFingerprint = JsonSerializer.Serialize(issue);
                issue.Fingerprint = ComputeSha1Hash(jsonForFingerprint);
                issues.Add(issue);
            }
        }

        var json = JsonSerializer.Serialize(issues.Distinct(), new JsonSerializerOptions {
            WriteIndented = true
        });
        ((AbsolutePath)settings.OutputFile).TouchFile();

        Encoding encoding = new UTF8Encoding(false);
        if (settings.CodePage != null && Encoding.GetEncoding(settings.CodePage.Value) != null) {
            encoding = Encoding.GetEncoding(settings.CodePage.Value);
        }

        File.WriteAllText(settings.OutputFile, json, encoding);
    }

    private static string ComputeSha1Hash(string text)
    {
        byte[]? data = Encoding.ASCII.GetBytes(text);
        
        byte[] hash = SHA1.HashData(data);
        var formatted = new StringBuilder(2 * hash.Length);

        foreach (var b in hash)
        {
            formatted.Append($"{b:x2}");
        }

        return formatted.ToString();
    }
}