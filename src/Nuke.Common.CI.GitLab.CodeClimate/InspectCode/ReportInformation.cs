﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

#nullable disable

namespace Nuke.Common.CI.GitLab.CodeClimate.InspectCode;

/// <remarks />
[Serializable]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
public class ReportInformation
{
    private ReportInformationInspectionScope inspectionScopeField;

    private string solutionField;

    /// <remarks />
    public string Solution
    {
        get => solutionField;
        set => solutionField = value;
    }

    /// <remarks />
    public ReportInformationInspectionScope InspectionScope
    {
        get => inspectionScopeField;
        set => inspectionScopeField = value;
    }
}