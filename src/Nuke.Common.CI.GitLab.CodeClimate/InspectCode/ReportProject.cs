﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

#nullable disable

namespace Nuke.Common.CI.GitLab.CodeClimate.InspectCode;

/// <remarks />
[Serializable]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
public class ReportProject
{
    private ReportProjectIssue[] issueField;

    private string nameField;

    /// <remarks />
    [XmlElement("Issue")]
    public ReportProjectIssue[] Issue
    {
        get => issueField;
        set => issueField = value;
    }

    /// <remarks />
    [XmlAttribute]
    public string Name
    {
        get => nameField;
        set => nameField = value;
    }
}