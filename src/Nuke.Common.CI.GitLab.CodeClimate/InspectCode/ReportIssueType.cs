﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

#nullable disable

namespace Nuke.Common.CI.GitLab.CodeClimate.InspectCode;

/// <remarks />
[Serializable]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
public class ReportIssueType
{
    private string categoryField;

    private string categoryIdField;

    private string descriptionField;

    private string globalField;

    private string idField;

    private InspectCodeSeverity severityField;

    private string subCategoryField;

    private string wikiUrlField;

    /// <remarks />
    [XmlAttribute]
    public string Id
    {
        get => idField;
        set => idField = value;
    }

    /// <remarks />
    [XmlAttribute]
    public string Category
    {
        get => categoryField;
        set => categoryField = value;
    }

    /// <remarks />
    [XmlAttribute]
    public string CategoryId
    {
        get => categoryIdField;
        set => categoryIdField = value;
    }

    /// <remarks />
    [XmlAttribute]
    public string Description
    {
        get => descriptionField;
        set => descriptionField = value;
    }

    /// <remarks />
    [XmlAttribute]
    public InspectCodeSeverity Severity
    {
        get => severityField;
        set => severityField = value;
    }

    /// <remarks />
    [XmlAttribute]
    public string WikiUrl
    {
        get => wikiUrlField;
        set => wikiUrlField = value;
    }

    /// <remarks />
    [XmlAttribute]
    public string SubCategory
    {
        get => subCategoryField;
        set => subCategoryField = value;
    }

    /// <remarks />
    [XmlAttribute]
    public string Global
    {
        get => globalField;
        set => globalField = value;
    }
}