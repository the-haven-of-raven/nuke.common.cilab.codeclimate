﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

#nullable disable

namespace Nuke.Common.CI.GitLab.CodeClimate.InspectCode;

/// <remarks />
[Serializable]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
public class ReportInformationInspectionScope
{
    private string elementField;

    /// <remarks />
    public string Element
    {
        get => elementField;
        set => elementField = value;
    }
}