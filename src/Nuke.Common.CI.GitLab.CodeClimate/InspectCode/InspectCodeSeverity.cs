﻿using System.Xml.Serialization;

namespace Nuke.Common.CI.GitLab.CodeClimate;

public enum InspectCodeSeverity
{
    [XmlEnum("DO_NOT_SHOW")]
    DoNotShow,
    [XmlEnum("HINT")]
    Hint,
    [XmlEnum("SUGGESTION")]
    Suggestion,
    [XmlEnum("WARNING")]
    Warning,
    [XmlEnum("ERROR")]
    Error
}