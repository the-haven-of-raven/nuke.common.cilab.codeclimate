﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

#nullable disable

namespace Nuke.Common.CI.GitLab.CodeClimate.InspectCode;

// NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
/// <remarks />
[Serializable]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class Report
{
    private ReportInformation informationField;

    private ReportProject[] issuesField;

    private ReportIssueType[] issueTypesField;

    private string toolsVersionField;

    /// <remarks />
    public ReportInformation Information
    {
        get => informationField;
        set => informationField = value;
    }

    /// <remarks />
    [XmlArrayItem("IssueType", IsNullable = false)]
    public ReportIssueType[] IssueTypes
    {
        get => issueTypesField;
        set => issueTypesField = value;
    }

    /// <remarks />
    [XmlArrayItem("Project", IsNullable = false)]
    public ReportProject[] Issues
    {
        get => issuesField;
        set => issuesField = value;
    }

    /// <remarks />
    [XmlAttribute]
    public string ToolsVersion
    {
        get => toolsVersionField;
        set => toolsVersionField = value;
    }
}