﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

#nullable disable

namespace Nuke.Common.CI.GitLab.CodeClimate.InspectCode;

/// <remarks />
[Serializable]
[DesignerCategory("code")]
[XmlType(AnonymousType = true)]
public class ReportProjectIssue
{
    private string fileField;

    private int lineField;

    private bool lineFieldSpecified;

    private string messageField;

    private string offsetField;

    private string typeIdField;

    /// <remarks />
    [XmlAttribute]
    public string TypeId
    {
        get => typeIdField;
        set => typeIdField = value;
    }

    /// <remarks />
    [XmlAttribute]
    public string File
    {
        get => fileField;
        set => fileField = value;
    }

    /// <remarks />
    [XmlAttribute]
    public string Offset
    {
        get => offsetField;
        set => offsetField = value;
    }

    /// <remarks />
    [XmlAttribute]
    [DefaultValue(1)]
    public int Line
    {
        get => lineField;
        set => lineField = value;
    }

    /// <remarks />
    [XmlIgnore]
    public bool LineSpecified
    {
        get => lineFieldSpecified;
        set => lineFieldSpecified = value;
    }

    /// <remarks />
    [XmlAttribute]
    public string Message
    {
        get => messageField;
        set => messageField = value;
    }
}