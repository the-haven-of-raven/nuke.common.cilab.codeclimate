﻿using System;
using System.Text.Json.Serialization;

namespace Nuke.Common.CI.GitLab.CodeClimate;

[Serializable]
public class Location
{
    public Location()
    {
        Path = string.Empty;
        Lines = new Lines();
        Positions = new Positions();
    }

    protected bool Equals(Location other)
    {
        return Path == other.Path && Lines.Equals(other.Lines) && Positions.Equals(other.Positions);
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) {
            return false;
        }

        if (ReferenceEquals(this, obj)) {
            return true;
        }

        if (obj.GetType() != this.GetType()) {
            return false;
        }

        return Equals((Location)obj);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Path, Lines, Positions);
    }

    [JsonPropertyName("path")]
    public string Path
    {
        get;
        set;
    } = default!;

    [JsonPropertyName("lines")]
    public Lines Lines
    {
        get;
        set;
    } = default!;

    [JsonPropertyName("positions")]
    public Positions Positions
    {
        get;
        set;
    } = default!;
}