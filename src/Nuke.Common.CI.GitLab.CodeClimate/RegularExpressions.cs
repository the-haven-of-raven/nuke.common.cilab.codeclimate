﻿using JetBrains.Annotations;

namespace Nuke.Common.CI.GitLab.CodeClimate;

[PublicAPI]
public static class RegularExpressions
{
    public static readonly string DotNetBuildWarning =
        "^(?<filename>.*)(\\((?<line>\\d+),(?<column>\\d+)\\)):\\s?(?<warning>warning\\s?(?<code>.*):\\s?(?<description>.*\\[(?<project>.*)\\]))$";
}