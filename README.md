# How to Compile

## Using Visual Studio 2019

This solution uses modern project file format, therefore VS2019 is required.

Open [Tesselate.sln](src/Tesselate.sln) in Visual Studio 2019.

## Using [NUKE](https://nuke.build/index.html) build

To use NUKE build automation tool you need to have .NET Core SDK installed, at least version 3.1 installed. (Comes with latest Visual Studio 2019) 

Open either command line or powershell and run following commands in the root of this repository.

From Windows command line:
```powershell
build [[-BuildArguments] <string[] BuildTargets>] [<CommonParameters>]
```

From PowerShell
```powershell
.\build.ps1 [[-BuildArguments] <string[] BuildTargets>] [<CommonParameters>]
```

From shell
```shell
.\build.sh [[-BuildArguments] <string[] BuildTargets>] [<CommonParameters>]
```

Following targets are avaible to use, these are called by CI builds.

Targets (with their direct dependencies):
```
  Clean               
  Restore             
  Compile (default)    -> Restore
  Pack                 -> Compile
  Push                 -> Pack
```
Parameters:
```
  --configuration      Configuration to build - Default is 'Debug' (local) or 'Release' (server).
  --nuget-api-key      NuGet API key.
  --nuget-url          NuGet URL. Default: 'https://api.nuget.org/v3/index.json'.

  --continue           Indicates to continue a previously failed build attempt.
  --help               Shows the help text for this build assembly.
  --host               Host for execution. Default is 'automatic'.
  --target             List of targets to be executed. Default is 'Compile'.
  --no-logo            Disables displaying the NUKE logo.
  --plan               Shows the execution plan (HTML).
  --root               Root directory during build execution.
  --skip               List of targets to be skipped. Empty list skips all dependencies.
  --verbosity          Logging verbosity during build execution Default is 'Normal'.
```