﻿using System;
using System.Linq;
using Nuke.Common;
using Nuke.Common.CI;
using Nuke.Common.CI.GitLab;
using Nuke.Common.CI.GitLab.CodeClimate;
using Nuke.Common.Execution;
using Nuke.Common.Git;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.GitVersion;
using Nuke.Common.Utilities.Collections;
using static Nuke.Common.CI.GitLab.CodeClimate.CodeClimateTasks;
using static Nuke.Common.EnvironmentInfo;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.IO.PathConstruction;
using static Nuke.Common.Tools.DotNet.DotNetTasks;
using static Nuke.Common.Tooling.ToolPathResolver;
using static Nuke.Common.Tooling.ProcessTasks;
using System.Text;

partial class Build
{
    AbsolutePath InspectCodeClimateWarningFile => RootDirectory / "code_quality_inspect_warnings_report.json";

    Target InspectCode => _ => _
        .Executes(() =>
        {
            DotNetToolRestore();
            DotNet($"jb inspectcode {Solution.Path} -o=\"inspectcode.xml\" --no-build");
            InspectCodeToCodeClimate("inspectcode.xml", s => s
                                         .SetDefaultSeverity(Severity.Disable)
                                         .SetSeverity(InspectCodeSeverity.Warning, Severity.Major)
                                         .SetSeverity(InspectCodeSeverity.Error, Severity.Critical)
                                         .SetCodePage(Encoding.Default.CodePage)
                                         .EnableIgnoreCSharpWarnings()
                                         .SetOutputFile(InspectCodeClimateWarningFile));
        });
}