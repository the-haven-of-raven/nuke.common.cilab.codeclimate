using System;
using System.IO;
using System.Linq;
using Nuke.Common;
using Nuke.Common.CI;
using Nuke.Common.CI.GitLab;
using Nuke.Common.CI.GitLab.CodeClimate;
using Nuke.Common.Execution;
using Nuke.Common.Git;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotCover;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.GitVersion;
using Nuke.Common.Tools.ReportGenerator;
using Nuke.Common.Utilities;
using Nuke.Common.Utilities.Collections;
using static Nuke.Common.CI.GitLab.CodeClimate.CodeClimateTasks;
using static Nuke.Common.EnvironmentInfo;
using static Nuke.Common.ChangeLog.ChangelogTasks;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.IO.PathConstruction;
using static Nuke.Common.Tools.DotNet.DotNetTasks;
using static Nuke.Common.Tools.DotCover.DotCoverTasks;
using static Nuke.Common.Tools.ReportGenerator.ReportGeneratorTasks;
using static Nuke.Common.ControlFlow;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Text.Json;
using System.Text;
using System.Security.Cryptography;

[UnsetVisualStudioEnvironmentVariables]
partial class Build : NukeBuild
{
    /// Support plugins are available for:
    ///   - JetBrains ReSharper        https://nuke.build/resharper
    ///   - JetBrains Rider            https://nuke.build/rider
    ///   - Microsoft VisualStudio     https://nuke.build/visualstudio
    ///   - Microsoft VSCode           https://nuke.build/vscode

    public static int Main () => Execute<Build>(x => x.Compile);

    [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
    readonly Configuration Configuration = IsLocalBuild ? Configuration.Debug : Configuration.Release;

    [Solution] readonly Solution Solution;
    [GitRepository] readonly GitRepository GitRepository;
    [GitVersion(Framework = "net5.0")] readonly GitVersion GitVersion;

    [CI] readonly GitLab GitLab;

    AbsolutePath SourceDirectory => RootDirectory / "src";
    AbsolutePath OutputDirectory => RootDirectory / "output";

    Target Clean => _ => _
        .Before(Restore)
        .Executes(() =>
        {
            SourceDirectory.GlobDirectories("**/bin", "**/obj").ForEach(x => x.DeleteDirectory());
            OutputDirectory.CreateOrCleanDirectory();
        });

    Target Restore => _ => _
        .Executes(() =>
        {
            DotNetRestore(s => s
                .SetProjectFile(Solution));
        });
    
    AbsolutePath CodeClimateWarningFile => RootDirectory / "code_quality_build_warnings_report.json";

    Target Compile => _ => _
        .DependsOn(Restore)
        .Executes(() =>
        {
            var output = DotNetBuild(s => s
                                          .SetProjectFile(Solution)
                                          .SetConfiguration(Configuration)
                                          .SetAssemblyVersion(GitVersion.AssemblySemVer)
                                          .SetFileVersion(GitVersion.AssemblySemFileVer)
                                          .SetInformationalVersion(GitVersion.InformationalVersion)
                                          .EnableNoRestore());

            OutputToCodeClimate(output, s => s
                                             .SetRegex(RegularExpressions.DotNetBuildWarning)
                                             .SetRootDirectory(RootDirectory)
                                             .SetOutputFile(CodeClimateWarningFile)
                                             .SetCodePage(Encoding.Default.CodePage));
        });

    string ChangelogFile => RootDirectory / "CHANGELOG.md";
    AbsolutePath PackageDirectory => OutputDirectory / "packages";

    Target Pack => _ => _
        .DependsOn(Compile)
        .Produces(PackageDirectory / "*.nupkg")
        .Executes(() =>
        {
            DotNetPack(s =>
            {
                s = s
                    .SetProject(Solution)
                    .EnableNoBuild()
                    .SetConfiguration(Configuration)
                    .SetOutputDirectory(PackageDirectory)
                    .SetVersion(GitVersion.FullSemVer)
                    .SetProcessArgumentConfigurator(args => 
                        args.Add("/nowarn:NU5105")
                            .Add("/warnaserror:NU5104,NU1605"));
                
                try {
                    var notes = GetNuGetReleaseNotes(ChangelogFile, GitRepository);
                    s = s.SetPackageReleaseNotes(notes);
                } catch (Exception) {
                    Serilog.Log.Write(Serilog.Events.LogEventLevel.Warning, "Empty changelog!");
                }

                return s;
            });
        });

    [Parameter("NuGet API key", Separator = "_")]
    readonly string NugetApiKey;

    [Parameter("URL or name of nuget source to publish tag builds. Default: 'https://api.nuget.org/v3/index.json'", Separator = "_")]
    readonly string NugetSource = "https://api.nuget.org/v3/index.json";

    [Parameter("URL or name of nuget source to publish every build. Default: 'gitlab'", Separator = "_")]
    readonly string PrivateNugetSource = "gitlab";
    
    Target Push => _ => _
        .DependsOn(Pack)
        .Requires(() => NugetSource)
        .Requires(() => PrivateNugetSource)
        .Executes(() => {
            DotNetNuGetPush(s => {
                s = s.SetTargetPath(PackageDirectory / "*.nupkg");
                if (!string.IsNullOrWhiteSpace(NugetApiKey)) {
                    s = s.SetApiKey(NugetApiKey);
                }
                if (!string.IsNullOrWhiteSpace(GitLab?.CommitTag)) {
                    s = s.SetSource(NugetSource);
                } else {
                    s = s.SetSource(PrivateNugetSource);
                }

                return s;
            });
        });
    
}
