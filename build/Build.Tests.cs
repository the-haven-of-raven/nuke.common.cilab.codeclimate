using System.IO;
using System.Linq;
using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.Coverlet;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.ReportGenerator;
using static Nuke.Common.EnvironmentInfo;
using static Nuke.Common.IO.PathConstruction;
using static Nuke.Common.Tools.Coverlet.CoverletTasks;
using static Nuke.Common.Tools.ReportGenerator.ReportGeneratorTasks;
using static Nuke.Common.ControlFlow;

partial class Build
{
    AbsolutePath TestResultsDirectory => OutputDirectory / "TestResults";

    [Parameter]
    public bool CreateCoverageReport = false;

    Target Coverage => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            var testProjects = Solution.GetAllProjects("*.Test").ToList();
            // This is so that the global dotnet is used instead of the one that comes with NUKE
            var dotnetPath = ToolPathResolver.GetPathExecutable("dotnet");

            for (var i = 0; i < testProjects.Count; i++)
            {
                var testProject = testProjects[i];
                var projectDirectory = Path.GetDirectoryName(testProject);
                var index = i;
                var frameworks = testProject.GetTargetFrameworks();

                SuppressErrors(() =>
                {
                    Coverlet(c => c
                        .SetFormat(CoverletOutputFormat.cobertura)
                        .CombineWith(frameworks, (oo, fm) => oo
                            .SetAssembly((AbsolutePath)projectDirectory / "bin" / Configuration / fm / $"{testProject.Name}.dll")
                            .SetOutput(OutputDirectory / $"cobertura_{index:00}_{fm}.xml")
                            .SetTargetSettings(
                                new DotNetTestSettings()
                                    .SetProcessToolPath(dotnetPath)
                                    .SetProjectFile(testProject)
                                    .SetProcessWorkingDirectory(projectDirectory)
                                    .SetResultsDirectory(TestResultsDirectory)
                                    .EnableNoBuild()
                                    .EnableNoRestore()
                                    .SetConfiguration(Configuration)
                                    .SetVerbosity(DotNetVerbosity.Normal)
                                    .SetLoggers($"junit;LogFilePath={TestResultsDirectory / $"TestResult_{index:00}_{fm}.xml"}"))));
                });
            }
        });
}